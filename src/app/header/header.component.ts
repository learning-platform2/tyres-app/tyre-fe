import { MediaMatcher } from '@angular/cdk/layout';
import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

/** @title Responsive sidenav */

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn: Boolean = false;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.isLogged
      .subscribe(
        data => {
          this.isLoggedIn = data;
        }
      );
  }

  logout() {
    console.log('logout');
    
    this.userService.userLogout();
    this.isLoggedIn=false;
  }
}