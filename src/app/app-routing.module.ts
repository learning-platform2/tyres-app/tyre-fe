import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminAccessComponent } from './admin-access/admin-access.component';
import { AppLoginComponent } from './app-login/app-login.component';
import { AuthGuard } from './auth.guard';
import { TyresComponent } from './tyres/tyres.component';


const routes: Routes = [
  { path: '', redirectTo: "tyres", pathMatch: "full" },
  { path: 'tyres', component:TyresComponent},
  { path: 'login', component: AppLoginComponent },
  { path: 'data-ie',component:AdminAccessComponent,canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
