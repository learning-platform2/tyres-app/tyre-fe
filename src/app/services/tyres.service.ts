import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Tyre } from '../models/Tyre.model';

@Injectable({
  providedIn: 'root'
})
export class TyresService {


  constructor(private http: HttpClient) { }



  url: string = 'http://localhost:8080/api/v1/tyre';


  setTyres(tyres: Tyre[]) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return this.http.post<Tyre[]>(this.url + "/all/accept", tyres, { headers: headers });
  }

  getTyres() {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return this.http.get<any>(this.url + "/all",{headers:headers});
  }

  updateTyre(id: string, tyre: Tyre) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return this.http.put<Tyre>(this.url+"/stock", tyre,{headers:headers});
  }

  deleteTyre(tyreId) {
    
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return this.http.delete<Tyre>
      (this.url+"/stock/"+tyreId,{headers:headers});
  }

  postTyre(tyre: Tyre) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return this.http.post<Tyre>(this.url, tyre,{headers:headers});
  }

  
  getTyre(id: string) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    return this.http.get<Tyre>(this.url + id,{headers:headers});
  }

}
