import { error } from '@angular/compiler/src/util';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { User } from '../models/User.model';

export interface userDetails {
  userId: number,
  userName: String,
  userPassword: String,
  expiresIn: any,
  role: any
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  users: userDetails[] =
    [{ "userId": 1, "userName": "vivek", "userPassword": "Vivek@8901", "expiresIn": "", "role": "admin" }]


  public user = new BehaviorSubject<userDetails>(null);
  public isLogged = new BehaviorSubject<Boolean>(false);

  public authenticationError = new BehaviorSubject<string>(null);

  constructor(private http: HttpClient, private router: Router) { }


  userSignin(user: User) {
    let userExist = this.users.filter(u =>
      u.userName === user.userName && u.userPassword === user.userPassword);

    if (userExist.length == 1) {
      localStorage.setItem("userData", JSON.stringify(userExist[0].userName));
      this.user.next(userExist[0]);
      this.isLogged.next(true);
      this.router.navigate(["/data-ie"]);
      this.authenticationError.next(null);
    } else {
      this.authenticationError.next("Incorrect UserName Password");
    }
  }

  userLogout() {
    localStorage.removeItem('userData')
    this.user.next(null);
    this.isLogged.next(false);
    this.router.navigate([""]);
  }

  // auto login even after refreshing page using local storage
  autoSignin() {
    const user = JSON.parse(localStorage.getItem('userData'));
    if (user) {
      this.user.next(user);
      this.isLogged.next(true);
    }
  }

}

