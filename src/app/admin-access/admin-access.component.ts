
import { Component, OnInit, ViewChild } from '@angular/core';

import * as XLSX from 'xlsx';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent } from '../dialog-box/dialog-box.component';
import { TyresService } from '../services/tyres.service';
import { TyreData } from '../tyres/tyres.component';
import { Tyre } from '../models/Tyre.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';

export interface UsersData {
  name: string;
  id: number;
}

type AOA = any[][];

@Component({
  selector: 'app-admin-access',
  templateUrl: './admin-access.component.html',
  styleUrls: ['./admin-access.component.css']
})
export class AdminAccessComponent implements OnInit {

  base64File: string = null;
  filename: string = null;

  data: AOA;
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  tyre: Tyre;
  tyreArray: Tyre[] = [];


  displayedColumns: string[] = ['description', 'stockQuantity', 'unitPrice', 'action'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  constructor(public dialog: MatDialog, private tyreService: TyresService
    , private matSnackBar: MatSnackBar) { }

  ngOnInit() {
    this.updateState();
  }

  updateState() {
    this.tyreService.getTyres().subscribe(
      result => {
        //this.snakBarpop(result.message)
        this.dataSource = new MatTableDataSource(result);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      err => {
        this.snakBarpop(err.error.message);
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  onFileChange(evt: any) {
    try {
      const file = evt.target.files[0];
      const fReader = new FileReader()
      fReader.readAsDataURL(file)
      fReader.onloadend = (_event: any) => {
        this.filename = file.name;
        this.base64File = _event.target.result;
      }
    } catch (error) {
      this.filename = null;
      this.base64File = null;
      this.snakBarpop('no file was selected...');
    }
    this.tyreArray = [];
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
     // console.log(this.data);
      this.data.forEach((element, index) => {
        if (index > 0) {
          this.tyre = new Tyre(null, element[0], element[1], element[2]);
          this.tyreArray.push(this.tyre)
        }

      });
     // console.log(this.tyreArray);

    };
    reader.readAsBinaryString(target.files[0]);
  }


  export(): void {
    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.dataSource);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }

  Conform() {
    let data = this.tyreArray.map(i => {
      return i;
    });

    this.tyreService.setTyres(data).subscribe(
      result => {
        this.dataSource = new MatTableDataSource(result);
        this.snakBarpop('Sucessfully uploaded');
        this.updateState();

      },
      err => {
        this.snakBarpop(err.message);
      }
    );

  }

  snakBarpop(str: string) {
    this.matSnackBar.open(str, 'Close', {
      duration: 4000
    });
  }

  openDialog(action, obj) {
   // console.log(obj, action);

    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Add') {
        this.addRowData(result.data);
      } else if (result.event == 'Update stock') {
        this.updateRowData(result.data);
      } else if (result.event == 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }

  addRowData(row_obj) {
    // var d = new Date();
    // this.dataSource.push({
    //   id: d.getTime(),
    //   name: row_obj.name
    // });
    // this.table.renderRows();

  }
  updateRowData(obj: Tyre) {
    this.tyreService.updateTyre(obj.id, new Tyre(obj.id, obj.description, obj.stockQuantity, obj.unitPrice)).subscribe(
      res => {
        this.updateState();
        this.snakBarpop('Sucessfully updated quantity')
      },
      err => {
        this.snakBarpop(err.error.message);
      }
    );
  }
  deleteRowData(obj: Tyre) {
    this.tyreService.deleteTyre(obj.id).subscribe(res => {
     // console.log(res);

      this.updateState();
      this.snakBarpop('Sucessfully deleted tyre ' + obj.description);
    },
      err => {
        this.updateState();
     //   console.log(err);
        this.snakBarpop("Deleted Tyre "+obj.description);
      });
  }
}
