import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Tyre } from '../models/Tyre.model';
import { TyresService } from '../services/tyres.service';
            

export interface TyreData {
  id: number;
  description: string;
  stockQuantity: number;
  unitPrice: number;
}


@Component({
  selector: 'app-tyres',
  templateUrl: './tyres.component.html',
  styleUrls: ['./tyres.component.css']
})
export class TyresComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['description', 'stockQuantity', 'unitPrice'];
  dataSource: MatTableDataSource<TyreData>;

  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  tyres: Tyre[] = [];

  constructor(private tyreService: TyresService,private matSnackBar: MatSnackBar) { }

  ngOnInit() {

    this.tyreService.getTyres().subscribe(
      result => {
     //  console.log(result);
       
        this.dataSource = new MatTableDataSource(result);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      err=>{       
       this.snakBarpop(err.error.message);
      });
  }

  ngAfterViewInit(): void {
  }

  snakBarpop(str:string){
    this.matSnackBar.open(str, 'Close', {
      duration: 4000
    });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}


