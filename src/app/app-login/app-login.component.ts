import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from '../models/User.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-app-login',
  templateUrl: './app-login.component.html',
  styleUrls: ['./app-login.component.css']
})
export class AppLoginComponent implements OnInit {

  constructor(private userService: UserService, private matSnackBar: MatSnackBar) { }

  error: string;

  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  logIn() {
    this.userService.userSignin(new User(this.form.value.username, this.form.value.password));
  }

  ngOnInit() {
    this.userService.authenticationError.subscribe(err => {
      if (err != null) {
        this.error = err
        this.matSnackBar.open(this.error, 'Close', {
          duration: 2000
        });

      }
    });
  }
}

