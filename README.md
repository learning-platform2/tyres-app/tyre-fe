# StockApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.9.

## To start server

* Install Node JS 

   windows  : https://nodejs.org/dist/v14.17.4/node-v14.17.4-x64.msi 

   Linux    : https://www.geeksforgeeks.org/installation-of-node-js-on-linux/

* Install angular CLI command :- npm install -g @angular/cli
* Run cd Tyre-FE to switch into working Directory
* Run npm install 
* Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will reload.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
